#ifndef __RESOURCE_jsunit_resources_H__
#define __RESOURCE_jsunit_resources_H__

#include <gio/gio.h>

extern GResource *jsunit_resources_get_resource (void);
#endif
