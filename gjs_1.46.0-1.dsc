-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: gjs
Binary: gjs, gjs-tests, libgjs0e, libgjs-dev
Architecture: any
Version: 1.46.0-1
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Iain Lane <laney@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: https://wiki.gnome.org/Projects/Gjs
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gjs
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/gjs
Testsuite: autopkgtest
Testsuite-Triggers: build-essential, dbus, dbus-x11, gnome-desktop-testing, xauth, xvfb
Build-Depends: debhelper (>= 10), dpkg-dev (>= 1.17.14), gnome-common, gnome-pkg-tools, pkg-config, libglib2.0-dev (>= 2.42.0), libgirepository1.0-dev (>= 1.41.4), gobject-introspection (>= 1.41.4), libmozjs-24-dev, libreadline-dev, libgtk-3-dev, libcairo2-dev, dbus <!nocheck>, dbus-x11 <!nocheck>, at-spi2-core <!nocheck>, xvfb <!nocheck>
Package-List:
 gjs deb interpreters optional arch=any
 gjs-tests deb interpreters optional arch=any
 libgjs-dev deb libdevel optional arch=any
 libgjs0e deb libs optional arch=any
Checksums-Sha1:
 7fcc71979563b5a587ab61f23f19ce258a9cf0eb 490812 gjs_1.46.0.orig.tar.xz
 68e56cb1cc8859279e20131a4829ce05243e3911 9596 gjs_1.46.0-1.debian.tar.xz
Checksums-Sha256:
 2283591fa70785443793e1d7db66071b36052d707075f229baeb468d8dd25ad4 490812 gjs_1.46.0.orig.tar.xz
 0515eea5779c166c7ec6eac3799aba208d7af09258a6963f032f22e8985e7856 9596 gjs_1.46.0-1.debian.tar.xz
Files:
 25574b60f4e1173fee0bd4920231df3e 490812 gjs_1.46.0.orig.tar.xz
 4db06a42815145e224383b23f0a48987 9596 gjs_1.46.0-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIuBAEBCAAYBQJX4az5ERxiaWVibEBkZWJpYW4ub3JnAAoJEGrh3w1gjyLcjIMP
/ihDmu+mHWMCgzpTOFzz9bwl7LF3S6siM9jnthypXrVJ34lf9CDIoVezGQV0+9yf
1K46yrPL0wl29XcLofvCs7NazCFz9nB2ThfrmQ+VOEHAggc5VetpH+pWHy66Obpt
mcbeuEe2K5klzdHkHtRQwJcpuCePShnkrzVVqgcs0O8qAlze2LnWu8TUgpA1EGn1
HuPJJ/xZvx82Qxiwh5KTRPQdALCuDvavhBl6AlzOr4quuPm0dSB5lrKa3ZSp8QeT
UPrMJ2FoZMeEPsVi1rNUZXaqdD5aKbC8au991Df9BLyQoZjvnE/lnpxKhfCKpQsE
RoXkVfnFFROtgm0KMXxN7HC/K5O53RT/72hhWfITM0Kg6iq99Qv/yTA0i5MXTqCN
8yWRzYakIDoXkZwFs46YbR5BeI7LaTZHEYK2MPx7GSSLlsGkIq/AQC/1ORZVEFGi
j2pTnDDiKXsX9TIBIwiXXr1sVMDYf900REDb93+9eK+hYP/9RHe30NhfGVQSq35C
mUFkXHm3sGKrnOEoGDpFvpKvD0q2lEg+I2NPyPhR0Fqeie13/3bdbMyBBU+vJZ4k
S5Ob3BODIMIKdN3iKlTfV8aka9bEhjlk5S7kyKf+BvL+siafdFoVtct4lX5WT+F7
BbU/C/AoKzq/r7m0gupPOVOUZv9WokhssE3YccbLNUBC
=2a79
-----END PGP SIGNATURE-----
